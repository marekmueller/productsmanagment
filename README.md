# Products Managment

Package for product managment and API for client application.

## Install

Via Composer

``` bash
composer require "marekmueller/productsmanagment":"1.0.*"
```

## Usage

``` php
php artisan migrate
```