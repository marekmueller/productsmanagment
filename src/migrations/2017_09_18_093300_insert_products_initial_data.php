<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertProductsInitialData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('products')->insert([
            ['name' => 'Produkt 1', 'amount' => 4],
            ['name' => 'Produkt 2', 'amount' => 12],
            ['name' => 'Produkt 3', 'amount' => 0],
            ['name' => 'Produkt 4', 'amount' => 6],
            ['name' => 'Produkt 5', 'amount' => 2],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {}
}
