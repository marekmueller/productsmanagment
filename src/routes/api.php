<?php

Route::namespace('Marekmueller\ProductsManagment\Http\Controllers\Api\V1')
    ->prefix('api/v1')
    ->group(function () {
        Route::resource('products', 'ProductController', ['only' => [
            'index', 'store', 'update', 'destroy'
        ]]);
    }
);
