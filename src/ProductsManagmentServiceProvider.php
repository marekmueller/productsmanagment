<?php

namespace Marekmueller\ProductsManagment;

use Illuminate\Support\ServiceProvider;

class ProductsManagmentServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        // Load api routes
        $this->loadRoutesFrom(__DIR__.'/routes/api.php');

        // Add product table migrations
        $this->loadMigrationsFrom(__DIR__.'/migrations');
    }
}