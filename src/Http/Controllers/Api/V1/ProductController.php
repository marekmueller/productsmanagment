<?php

namespace Marekmueller\ProductsManagment\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use Marekmueller\ProductsManagment\Http\Services\ProductService;
use Marekmueller\ProductsManagment\Http\Repositories\ProductRepository;
use Marekmueller\ProductsManagment\Http\Requests\GetProductsListRequest;
use Marekmueller\ProductsManagment\Http\Requests\StoreProductRequest;
use Marekmueller\ProductsManagment\Http\Requests\UpdateProductRequest;

class ProductController extends Controller
{
    private $productService;

    public function __construct()
    {
        $this->productService = new ProductService(new ProductRepository);
    }

    /**
     * Returns list of products.
     *
     * @param GetProductsListRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(GetProductsListRequest $request)
    {
        return $this->productService->getProducts($request->amount);
    }

    /**
     * Stores product in database.
     *
     * @param StoreProductRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request)
    {
        $data = $request->only('name', 'amount');

        $this->productService->store($data);

        return response('Product stored', 200);
    }

    /**
     * Updates product in database.
     *
     * @param  UpdateProductRequest $request
     * @param  String $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, $id)
    {
        $data = $request->only('name', 'amount');

        $this->productService->update($data, $id);

        return response('Product updated', 200);
    }

    /**
     * Removes product from database.
     *
     * @param  String $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->productService->destroy($id);

        return response('Product deleted', 200);
    }
}