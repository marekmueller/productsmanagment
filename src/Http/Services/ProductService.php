<?php

namespace Marekmueller\ProductsManagment\Http\Services;

use Marekmueller\ProductsManagment\Product;
use Marekmueller\ProductsManagment\Http\Contracts\ProductRepositoryInterface;

class ProductService {

    private $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * Returns list of products depending on required amount
     *
     * @param  String $amount
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function getProducts($amount)
    {
        if (null === $amount) {
            return $this->productRepository->getAllOnStock();

        } elseif (0 === (int) $amount) {
            return $this->productRepository->getAllOutOfStock();
        }

        return $this->productRepository->getAllByAmountOperator($amount, '>');
    }

    /**
     * Product store handler
     *
     * @param  Array $data
     */
    public function store($data)
    {
        $this->productRepository->store($data);
    }

    /**
     * Product update handler
     *
     * @param  Array $data
     * @param  String $id
     */
    public function update($data, $id)
    {
        $this->productRepository->update($data, $id);
    }

    /**
     * Product delete handler
     *
     * @param  String $id
     */
    public function destroy($id)
    {
        $this->productRepository->destroy($id);
    }
}