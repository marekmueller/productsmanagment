<?php

namespace Marekmueller\ProductsManagment\Http\Repositories;

use Marekmueller\ProductsManagment\Product;
use Marekmueller\ProductsManagment\Http\Contracts\ProductRepositoryInterface;

class ProductRepository implements ProductRepositoryInterface
{
    public function getAllOnStock()
    {
        return Product::where('amount', '>', 0)->get();
    }

    public function getAllOutOfStock()
    {
        return Product::where('amount', 0)->get();
    }

    public function getAllByAmountOperator($amount, $operator)
    {
        return Product::where('amount', $operator, $amount)->get();
    }

    public function store($data)
    {
        return Product::create($data);
    }

    public function update($data, $id)
    {
        return Product::findOrFail($id)->update($data);
    }

    public function destroy($id)
    {
        return Product::destroy($id);
    }
}