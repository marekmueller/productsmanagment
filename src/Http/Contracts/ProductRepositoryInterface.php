<?php

namespace Marekmueller\ProductsManagment\Http\Contracts;

interface ProductRepositoryInterface
{
    public function getAllOnStock();

    public function getAllOutOfStock();

    public function getAllByAmountOperator($amount, $operator);

    public function store($data);

    public function update($data, $id);

    public function destroy($id);
}