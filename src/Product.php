<?php

namespace Marekmueller\ProductsManagment;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = ['id'];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}